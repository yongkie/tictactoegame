package com.demo.tictactoegame.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.demo.tictactoegame.user.entity.UserEntity;
import com.demo.tictactoegame.user.model.UserModel;
import com.demo.tictactoegame.user.repository.UserRepository;
import com.demo.tictactoegame.user.service.UserService;
import com.demo.tictactoegame.user.service.UserServiceImpl;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
public class UserTest {

	@Autowired
	UserService userService;

	@Mock
	UserRepository userRepository;

	@Test
	void loadUserByUsername_ValidUsername_ExpectedUserDetails() {
		UserEntity data = new UserEntity();
		data.setUsername("test");
		data.setPassword(UserServiceImpl.PASSWORD_ENCODER.encode("test"));
		when(userRepository.findByUsername(anyString())).thenReturn(data);

		UserDetails userDetails = userService.loadUserByUsername("test");
		
		userRepository.findByUsername(data.getUsername());
		verify(userRepository).findByUsername(data.getUsername());

		assertThat(userDetails.getUsername()).isEqualTo(data.getUsername());
		assertThat(UserServiceImpl.PASSWORD_ENCODER.matches("test", userDetails.getPassword()));
		assertThat(userDetails.getAuthorities()).hasSize(1);

		GrantedAuthority actualAuthority = userDetails.getAuthorities().iterator().next();
		assertThat(actualAuthority.getAuthority()).isEqualTo("USER");
	}

	@Test
	void loadUserByUsername_InvalidUsername_ThrowException() {
		when(userRepository.findByUsername(anyString())).thenReturn(null);

		assertThatThrownBy(() -> userService.loadUserByUsername("test-username"))
				.isInstanceOf(UsernameNotFoundException.class).hasMessage("Invalid username: test-username");
	}
}
