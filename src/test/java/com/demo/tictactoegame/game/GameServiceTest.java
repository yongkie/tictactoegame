package com.demo.tictactoegame.game;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.demo.tictactoegame.game.board.util.BoardUtil;
import com.demo.tictactoegame.game.model.GameModel;
import com.demo.tictactoegame.game.model.GameModel.GameState;
import com.demo.tictactoegame.game.model.GameModel.PlayerNumber;
import com.demo.tictactoegame.game.model.GameModel.PlayerType;
import com.demo.tictactoegame.game.repository.GameRepository;
import com.demo.tictactoegame.game.service.GameService;
import com.demo.tictactoegame.user.entity.UserEntity;
import com.demo.tictactoegame.user.model.UserModel;
import com.demo.tictactoegame.user.service.UserService;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
public class GameServiceTest {

	@Autowired
	GameService service;

	@Autowired
	UserService userService;

	@Mock
	private GameRepository mockRepository;

	@Test
	void create_PlayerGoFirst_Player1TypeHuman() {
		boolean playerGoFirst = true;
		UserModel appUser = userService.findByUsername("test");

		GameModel game = service.createNewGame(appUser, 3, playerGoFirst);

		assertThat(game.getUser().getUsername()).isEqualTo(appUser.getUsername());
		assertThat(game.getState()).isEqualTo(GameState.IN_PROGRESS);
		assertThat(game.getNextMove()).isEqualTo(PlayerNumber.PLAYER_1);
		assertThat(game.getPlayer1Type()).isEqualTo(PlayerType.HUMAN);
		assertThat(game.getPlayer2Type()).isEqualTo(PlayerType.COMPUTER);
		assertThat(game.getRows()).isEqualTo(BoardUtil.createEmpty(3, 3));

		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(game, entity);
		mockRepository.deleteUserGames(entity);
		verify(mockRepository).deleteUserGames(entity);
	}

	@Test
	void create_PlayerGoSecond_Player2TypeHuman() {
		boolean playerGoFirst = false;
		UserModel appUser = userService.findByUsername("test");

		GameModel game = service.createNewGame(appUser, 3, playerGoFirst);

		assertThat(game.getUser().getUsername()).isEqualTo(appUser.getUsername());
		assertThat(game.getState()).isEqualTo(GameState.IN_PROGRESS);
		assertThat(game.getNextMove()).isEqualTo(PlayerNumber.PLAYER_1);
		assertThat(game.getPlayer1Type()).isEqualTo(PlayerType.COMPUTER);
		assertThat(game.getPlayer2Type()).isEqualTo(PlayerType.HUMAN);
		assertThat(game.getRows()).isEqualTo(BoardUtil.createEmpty(3, 3));

		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(game, entity);
		mockRepository.deleteUserGames(entity);
		verify(mockRepository).deleteUserGames(entity);
	}

	@Test
	void getLastGame_Call_DelegateToRepository() {
		UserModel appUser = userService.findByUsername("test");
		GameModel game = service.getLastGame(appUser);

		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(game, entity);
		mockRepository.findFirstByUserOrderByIdDesc(entity);
		verify(mockRepository).findFirstByUserOrderByIdDesc(entity);
	}

	@Test
	void takeTurn_NewGameTwoMoves_GameInProgress() {
		UserModel appUser = userService.findByUsername("test");
		GameModel game = service.createNewGame(appUser, 3, true);

		service.takeTurn(game, "1-1");

		assertThat(game.getNextMove()).isEqualTo(PlayerNumber.PLAYER_2);
		assertThat(game.getState()).isEqualTo(GameState.IN_PROGRESS);
		assertRows(//@formatter:off
                game,
                "", "", "",
                "", "x", "",
                "", "", ""
        );//@formatter:on

		service.takeTurn(game, "0-1");

		assertThat(game.getNextMove()).isEqualTo(PlayerNumber.PLAYER_1);
		assertThat(game.getState()).isEqualTo(GameState.IN_PROGRESS);
		assertRows(//@formatter:off
                game,
                "", "o", "",
                "", "x", "",
                "", "", ""
        );//@formatter:on
	}

	@Test
	void takeTurn_Player1Wins_GameOverPlayer1Win() {
		UserModel appUser = userService.findByUsername("test");
		GameModel game = service.createNewGame(appUser, 3, true);
		game.getRows().set(0, Arrays.asList("x", "x", ""));

		service.takeTurn(game, "0-2");

		assertThat(game.getNextMove()).isNull();
		assertThat(game.getState()).isEqualTo(GameState.PLAYER_1_WIN);
		assertRows(//@formatter:off
                game,
                "x", "x", "x",
                "", "", "",
                "", "", ""
        );//@formatter:on
	}

	@Test
	void takeTurn_Player2Wins_GameOverPlayer2Win() {
		UserModel appUser = userService.findByUsername("test");
		GameModel game = service.createNewGame(appUser, 3, true);
		game.getRows().set(0, Arrays.asList("x", "x", "o"));
		game.getRows().set(1, Arrays.asList("", "o", ""));
		game.getRows().set(2, Arrays.asList("", "", ""));

		service.takeTurn(game, "1-0"); // player 1 marks X
		service.takeTurn(game, "2-0"); // player 2 marks O and wins

		assertThat(game.getNextMove()).isNull();
		assertThat(game.getState()).isEqualTo(GameState.PLAYER_2_WIN);
	}

	@Test
	void takeTurn_NoWinners_GameOverDraw() {
		UserModel appUser = userService.findByUsername("test");
		GameModel game = service.createNewGame(appUser, 3, true);
		game.getRows().set(0, Arrays.asList("o", "x", "x"));
		game.getRows().set(1, Arrays.asList("x", "o", "o"));
		game.getRows().set(2, Arrays.asList("o", "", "x"));

		service.takeTurn(game, "2-1");

		assertThat(game.getNextMove()).isNull();
		assertThat(game.getState()).isEqualTo(GameState.DRAW);
	}

	/**
	 * Helper 
	 */
	private void assertRows(GameModel game, String... tiles) {
		List<List<String>> rows = game.getRows();
		assertThat(rows.get(0)).containsExactly(tiles[0], tiles[1], tiles[2]);
		assertThat(rows.get(1)).containsExactly(tiles[3], tiles[4], tiles[5]);
		assertThat(rows.get(2)).containsExactly(tiles[6], tiles[7], tiles[8]);
	}
}
