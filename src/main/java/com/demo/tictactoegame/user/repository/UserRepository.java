package com.demo.tictactoegame.user.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.demo.tictactoegame.user.entity.UserEntity;

@Transactional(readOnly = true)
@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, String> {

	UserEntity findByUsername(String username);

	Boolean existsByUsername(String username);
}
