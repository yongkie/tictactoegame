package com.demo.tictactoegame.user.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.demo.tictactoegame.user.entity.UserEntity;
import com.demo.tictactoegame.user.model.UserModel;
import com.demo.tictactoegame.user.repository.UserRepository;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

	public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		UserEntity users = userRepository.findByUsername(username);
		if (users == null) {
			throw new UsernameNotFoundException("Invalid username: " + username);
		}
		List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("USER");
		return new User(users.getUsername(), users.getPassword(), authorities);
	}

	@Override
	public UserModel createNewUser(UserModel data) {
		// TODO Auto-generated method stub
		UserEntity entity = new UserEntity();
		entity.setUsername(data.getUsername());
		entity.setPassword(PASSWORD_ENCODER.encode(data.getPassword()));

		entity = userRepository.save(entity);
		BeanUtils.copyProperties(entity, data);
		return data;
	}

	@Override
	public UserModel findByUsername(String username) {
		// TODO Auto-generated method stub
		UserEntity entity = userRepository.findByUsername(username);
		UserModel result = new UserModel();
		if (entity != null) {
			BeanUtils.copyProperties(entity, result);
		}
		return result;
	}

	@Override
	public Boolean existsByUsername(String username) {
		// TODO Auto-generated method stub
		return userRepository.existsByUsername(username);
	}
}
