package com.demo.tictactoegame.user.service;

import org.springframework.security.core.userdetails.UserDetails;

import com.demo.tictactoegame.user.model.UserModel;

public interface UserService {
	UserDetails loadUserByUsername(String username);
	
	UserModel createNewUser(UserModel data);

	UserModel findByUsername(String username);

	Boolean existsByUsername(String username);
}
