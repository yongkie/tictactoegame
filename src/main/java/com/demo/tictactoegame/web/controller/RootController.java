package com.demo.tictactoegame.web.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.tictactoegame.game.model.GameModel;
import com.demo.tictactoegame.game.model.GameModel.PlayerType;
import com.demo.tictactoegame.game.service.GameService;
import com.demo.tictactoegame.user.model.UserModel;
import com.demo.tictactoegame.user.service.UserService;

@Controller
public class RootController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	GameService gameService;

	@Autowired
	UserService userService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Principal principal, Model model) {
		UserModel user = this.getUser(principal);

		GameModel game = gameService.getLastGame(user);

		if (game.getId() == null) {
			game = gameService.createNewGame(user, 3, true);
		}

		setModelGameAttributes(model, game);

		return "index";
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public String takeTurns(Model model, Principal principal, @RequestParam("tile_id") String tileId,
			@RequestParam(value = "boardsize", required = true, defaultValue = "3") int boardSize,
			@RequestParam(value = "new_game", required = false, defaultValue = "false") boolean newGame,
			@RequestParam(value = "player_go_first", required = false, defaultValue = "false") boolean playerGoFirst) {
		UserModel user = this.getUser(principal);

		logger.info("LOGGED USER ::: {}", user.toString());

		logger.info("Board Size ::: {}", boardSize);

		GameModel game;
		if (newGame) {
			game = gameService.createNewGame(user, boardSize, playerGoFirst);
			if (!playerGoFirst) {
				// give computer a small advantage by always placing X in the center as its
				// first move
				gameService.takeTurn(game, "1-1");
			}
		} else {
			game = gameService.getLastGame(user);
			gameService.takeTurn(game, tileId); // Player Turn
			gameService.takeTurnRandom(game); // Computer Turn
		}

		logger.info("GAME ::: {}", game.toString());

		setModelGameAttributes(model, game);

		return "index";
	}

	private void setModelGameAttributes(Model model, GameModel game) {
		boolean playerGoFirst = game.getPlayer1Type() == PlayerType.HUMAN;

		String playerStatus;

		switch (game.getState()) {
		case PLAYER_1_WIN:
			playerStatus = playerGoFirst ? "WON" : "LOST";
			break;
		case PLAYER_2_WIN:
			playerStatus = playerGoFirst ? "LOST" : "WON";
			break;
		case DRAW:
			playerStatus = "DRAW";
			break;
		case IN_PROGRESS:
		default:
			playerStatus = "IN_PROGRESS";
			break;
		}

		model.addAttribute("playerGoFirst", playerGoFirst);
		model.addAttribute("playStatus", playerStatus);
		model.addAttribute("board", game.getRows());
	}

	private UserModel getUser(Principal principal) {
		UserModel user = userService.findByUsername(principal.getName());
		if (user == null) {
			throw new UsernameNotFoundException("Invalid username: " + principal.getName());
		}
		return user;
	}
}
