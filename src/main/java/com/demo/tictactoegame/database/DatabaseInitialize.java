package com.demo.tictactoegame.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import com.demo.tictactoegame.user.model.UserModel;
import com.demo.tictactoegame.user.service.UserService;

@Component
public class DatabaseInitialize implements CommandLineRunner {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	UserService userService;

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		this.createNewUser("test", "test");
		this.createNewUser("fwd", "fwd");
	}

	private void createNewUser(String username, String password) {
		if (!userService.existsByUsername(username)) {
			UserModel users = new UserModel();
			users.setUsername(username);
			users.setPassword(password);
			userService.createNewUser(users);

			logger.info("Created user ({}) with username '{}' and password '{}'", users.getId(), username, password);
		} else {
			logger.info("Username '{}' and password '{}' is already exists", username, password);
		}
	}
}
