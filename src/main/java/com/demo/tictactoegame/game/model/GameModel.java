package com.demo.tictactoegame.game.model;

import java.util.List;

import javax.persistence.Column;

import org.hibernate.annotations.Type;

import com.demo.tictactoegame.common.CommonUtil;
import com.demo.tictactoegame.user.model.UserModel;

public class GameModel {
	public enum PlayerType {
		HUMAN, COMPUTER;
	}

	public enum PlayerNumber {
		PLAYER_1, PLAYER_2, GAME
	}

	public enum GameState {
		IN_PROGRESS, PLAYER_1_WIN, PLAYER_2_WIN, DRAW
	}

	private String id;

	private UserModel user;

	private PlayerType player1Type;

	private PlayerType player2Type;

	private PlayerNumber nextMove;

	private GameState state;

	private List<List<String>> rows;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

	public PlayerType getPlayer1Type() {
		return player1Type;
	}

	public void setPlayer1Type(PlayerType player1Type) {
		this.player1Type = player1Type;
	}

	public PlayerType getPlayer2Type() {
		return player2Type;
	}

	public void setPlayer2Type(PlayerType player2Type) {
		this.player2Type = player2Type;
	}

	public PlayerNumber getNextMove() {
		return nextMove;
	}

	public void setNextMove(PlayerNumber nextMove) {
		this.nextMove = nextMove;
	}

	public GameState getState() {
		return state;
	}

	public void setState(GameState state) {
		this.state = state;
	}

	public List<List<String>> getRows() {
		return rows;
	}

	public void setRows(List<List<String>> rows) {
		this.rows = rows;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return CommonUtil.printPrettyJSON(this);
	}
}
