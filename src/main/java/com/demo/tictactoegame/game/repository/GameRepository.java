package com.demo.tictactoegame.game.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.demo.tictactoegame.game.entity.GameEntity;
import com.demo.tictactoegame.user.entity.UserEntity;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface GameRepository extends PagingAndSortingRepository<GameEntity, String> {

	Optional<GameEntity> findFirstByUserOrderByIdDesc(UserEntity user);

	@Modifying
	@Transactional
	@Query("DELETE FROM GameEntity WHERE user = :user")
	void deleteUserGames(@Param("user") UserEntity User);
}
