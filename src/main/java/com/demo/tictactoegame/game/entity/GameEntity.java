package com.demo.tictactoegame.game.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.demo.tictactoegame.game.model.GameModel.GameState;
import com.demo.tictactoegame.game.model.GameModel.PlayerNumber;
import com.demo.tictactoegame.game.model.GameModel.PlayerType;
import com.demo.tictactoegame.user.entity.UserEntity;
import com.vladmihalcea.hibernate.type.json.JsonType;

@Entity
@TypeDef(name = "json", typeClass = JsonType.class)
public class GameEntity {
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private String id;

	@ManyToOne
	private UserEntity user;

	private String player1Type;

	private String player2Type;

	private String nextMove;

	private String state;

	@Type(type = "json")
	@Column(columnDefinition = "json")
	private List<List<String>> rows;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public String getPlayer1Type() {
		return player1Type;
	}

	public void setPlayer1Type(String player1Type) {
		this.player1Type = player1Type;
	}

	public String getPlayer2Type() {
		return player2Type;
	}

	public void setPlayer2Type(String player2Type) {
		this.player2Type = player2Type;
	}

	public String getNextMove() {
		return nextMove;
	}

	public void setNextMove(String nextMove) {
		this.nextMove = nextMove;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<List<String>> getRows() {
		return rows;
	}

	public void setRows(List<List<String>> rows) {
		this.rows = rows;
	}

}
