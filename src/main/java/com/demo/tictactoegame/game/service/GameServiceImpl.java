package com.demo.tictactoegame.game.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.tictactoegame.game.board.util.BoardTile;
import com.demo.tictactoegame.game.board.util.BoardUtil;
import com.demo.tictactoegame.game.entity.GameEntity;
import com.demo.tictactoegame.game.model.GameModel;
import com.demo.tictactoegame.game.model.GameModel.GameState;
import com.demo.tictactoegame.game.model.GameModel.PlayerNumber;
import com.demo.tictactoegame.game.model.GameModel.PlayerType;
import com.demo.tictactoegame.game.repository.GameRepository;
import com.demo.tictactoegame.user.entity.UserEntity;
import com.demo.tictactoegame.user.model.UserModel;

@Service
public class GameServiceImpl implements GameService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	GameRepository gameRepository;

	@Override
	public GameModel saveGame(GameModel game) {
		// TODO Auto-generated method stub
		GameEntity entity = new GameEntity();
		BeanUtils.copyProperties(game, entity);
		UserEntity userEntity = new UserEntity();
		BeanUtils.copyProperties(game.getUser(), userEntity);
		entity.setUser(userEntity);
		if (game.getNextMove() != null) {
			entity.setNextMove(game.getNextMove().name());
		} else {
			entity.setNextMove(PlayerNumber.GAME.name());
		}
		entity.setState(game.getState().name());
		entity.setPlayer1Type(game.getPlayer1Type().name());
		entity.setPlayer2Type(game.getPlayer2Type().name());

		entity = gameRepository.save(entity);

		GameModel result = new GameModel();
		BeanUtils.copyProperties(entity, result);
		UserModel userdata = new UserModel();
		BeanUtils.copyProperties(entity.getUser(), userdata);
		result.setUser(userdata);
		result.setState(GameState.valueOf(entity.getState()));
		result.setNextMove(PlayerNumber.valueOf(entity.getNextMove()));
		result.setPlayer1Type(PlayerType.valueOf(entity.getPlayer1Type()));
		result.setPlayer2Type(PlayerType.valueOf(entity.getPlayer2Type()));
		return result;
	}

	@Override
	public GameModel createNewGame(UserModel user, int boardsize, boolean playerGoFirst) {
		// TODO Auto-generated method stub
		this.deleteUserGames(user);

		GameEntity entity = new GameEntity();

		UserEntity userEntity = new UserEntity();
		BeanUtils.copyProperties(user, userEntity);

		entity.setUser(userEntity);
		entity.setState(GameState.IN_PROGRESS.name());
		entity.setNextMove(PlayerNumber.PLAYER_1.name());

		if (playerGoFirst) {
			entity.setPlayer1Type(PlayerType.HUMAN.name());
			entity.setPlayer2Type(PlayerType.COMPUTER.name());
		} else {
			entity.setPlayer1Type(PlayerType.COMPUTER.name());
			entity.setPlayer2Type(PlayerType.HUMAN.name());
		}
		entity.setRows(BoardUtil.createEmpty(boardsize, boardsize));

		entity = gameRepository.save(entity);

		GameModel result = new GameModel();
		BeanUtils.copyProperties(entity, result);
		UserModel userdata = new UserModel();
		BeanUtils.copyProperties(entity.getUser(), userdata);
		result.setUser(userdata);
		result.setState(GameState.valueOf(entity.getState()));
		result.setNextMove(PlayerNumber.valueOf(entity.getNextMove()));
		result.setPlayer1Type(PlayerType.valueOf(entity.getPlayer1Type()));
		result.setPlayer2Type(PlayerType.valueOf(entity.getPlayer2Type()));
		return result;
	}

	@Override
	public GameModel getLastGame(UserModel user) {
		// TODO Auto-generated method stub
		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(user, entity);

		Optional<GameEntity> lastGame = gameRepository.findFirstByUserOrderByIdDesc(entity);

		GameModel result = new GameModel();
		lastGame.ifPresent(data -> {
			BeanUtils.copyProperties(data, result);
			UserModel users = new UserModel();
			BeanUtils.copyProperties(data.getUser(), users);
			result.setUser(user);
			result.setState(GameState.valueOf(data.getState()));
			result.setNextMove(PlayerNumber.valueOf(data.getNextMove()));
			result.setPlayer1Type(PlayerType.valueOf(data.getPlayer1Type()));
			result.setPlayer2Type(PlayerType.valueOf(data.getPlayer2Type()));
		});

		return result;
	}

	@Override
	public void deleteUserGames(UserModel user) {
		// TODO Auto-generated method stub
		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(user, entity);
		gameRepository.deleteUserGames(entity);
	}

	@Override
	public void takeTurnRandom(GameModel game) {
		// TODO Auto-generated method stub
		String tileId = BoardUtil.getRandomAvailableTile(game.getRows());
		if (tileId != null) {
			takeTurn(game, tileId);
		}
	}

	@Override
	public void takeTurn(GameModel game, String tileId) {
		// TODO Auto-generated method stub
		if (game.getState() != GameState.IN_PROGRESS) {
			return;
		}

		String[] indices = tileId.split("-");
		if (indices.length != 2) {
			return;
		}

		BoardTile tile;
		if (game.getNextMove() == PlayerNumber.PLAYER_1) {
			tile = BoardTile.X;
			game.setNextMove(PlayerNumber.PLAYER_2);
		} else {
			tile = BoardTile.O;
			game.setNextMove(PlayerNumber.PLAYER_1);
		}

		int rowIndex = Integer.parseInt(indices[0]);
		int columnIndex = Integer.parseInt(indices[1]);
		game.getRows().get(rowIndex).set(columnIndex, tile.toString());

		GameState state = evaluateGameState(game.getRows());
		game.setState(state);
		if (state != GameState.IN_PROGRESS) {
			game.setNextMove(null); // Game over. Null out
		}
		this.saveGame(game);
	}

	@Override
	public GameState evaluateGameState(List<List<String>> rows) {
		// TODO Auto-generated method stub
		for (List<String> line : BoardUtil.getAllLines(rows)) {
			String firstTile = line.get(0);
			if (firstTile.isEmpty()) {
				continue;
			}

			if (line.stream().allMatch(tile -> tile.equals(firstTile))) {
				return firstTile.equals(BoardTile.X.toString()) ? GameState.PLAYER_1_WIN : GameState.PLAYER_2_WIN;
			}
		}

		for (List<String> row : rows) {
			if (row.stream().anyMatch(String::isEmpty)) {
				return GameState.IN_PROGRESS;
			}
		}

		return GameState.DRAW;
	}
}
