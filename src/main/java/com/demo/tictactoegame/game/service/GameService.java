package com.demo.tictactoegame.game.service;

import java.util.List;

import com.demo.tictactoegame.game.model.GameModel;
import com.demo.tictactoegame.game.model.GameModel.GameState;
import com.demo.tictactoegame.user.model.UserModel;

public interface GameService {
	GameModel saveGame(GameModel game);
	
	GameModel createNewGame(UserModel user, int boardsize, boolean playerGoFirst);

	GameModel getLastGame(UserModel user);

	public void takeTurnRandom(GameModel game);

	public void takeTurn(GameModel game, String tileId);
	
	public GameState evaluateGameState(List<List<String>> rows);

	void deleteUserGames(UserModel user);
}
