package com.demo.tictactoegame.game.board.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class BoardUtil {

//	private static final int NUMBER_ROWS = 4;
//	private static final int NUMBER_COLUMNS = 4;

	public static List<List<String>> createEmpty(int rowSize, int columnSize) {
		List<List<String>> rows = new ArrayList<>();

		for (int rowIndex = 0; rowIndex < rowSize; rowIndex++) {
			List<String> row = new ArrayList<>();
			for (int columnIndex = 0; columnIndex < columnSize; columnIndex++) {
				row.add(BoardTile.EMPTY.toString());

			}
			rows.add(row);
		}

		return rows;
	}

	public static String getRandomAvailableTile(List<List<String>> rows) {
		List<String> available = new ArrayList<>();

		for (int rowIndex = 0; rowIndex < rows.size(); rowIndex++) {
			List<String> row = rows.get(rowIndex);

			for (int columnIndex = 0; columnIndex < rows.size(); columnIndex++) {
				String tileValue = row.get(columnIndex);
				if (tileValue.isEmpty()) {
					available.add(rowIndex + "-" + columnIndex);
				}
			}
		}

		if (available.isEmpty()) {
			return null;
		}

		int randomNum = new Random().nextInt(available.size());
		return available.get(randomNum);

	}

	public static List<List<String>> getAllLines(List<List<String>> rows) {
		List<List<String>> lines = new ArrayList<>();

		for (int rowIndex = 0; rowIndex < rows.get(0).size(); rowIndex++) {
			lines.add(rows.get(rowIndex));
		}

		for (int columnIndex = 0; columnIndex < rows.get(0).size(); columnIndex++) {
			List<String> columnLine = new ArrayList<>();
			for (List<String> row : rows) {
				columnLine.add(row.get(columnIndex));
			}
			lines.add(columnLine);
		}

		// check diagonal 1
		List<String> diagonal1 = new ArrayList<String>();
		for (int rowIndex = 0; rowIndex < rows.get(0).size(); rowIndex++) {
			diagonal1.add(rows.get(rowIndex).get(rowIndex));
		}
		lines.add(diagonal1);

		// check diagonal 2
		List<String> diagonal2 = new ArrayList<String>();
		for (int rowIndex = 0; rowIndex < rows.get(0).size(); rowIndex++) {
			diagonal2.add(rows.get(rowIndex).get((rows.get(0).size() - 1) - rowIndex));
		}
		lines.add(diagonal2);

		return lines;
	}
}
