# Tic Tac Toe

## Tech Stack
| | Technology |
|---|---|
| __Language__ | Java 1.8 |
| __Framework__ | Spring Boot (v2.5.9) |
| __Data Layer__ | Postgres Database, JPA & Hibernate | 
| __UI Layer__ | HTML, CSS, Javascript, jQuery (v3.6), [Bootstrap](https://getbootstrap.com/) (v5), [Thymeleaf](http://www.thymeleaf.org/) |
| __Testing__ | JUnit 5, Mockito, AssertJ |
| __Build Tool__ | Gradle (v7.4) |

## Install & Run
* Install Java 8.
* Clone repo: `git clone https://yongkie@bitbucket.org/yongkie/tictactoegame.git`
* Navigate `cd tictactoegame` and run applicable [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:using_wrapper) command:
  * macOS/Unix: `./gradlew bootRun`
  * Windows: `gradlew.bat bootRun`
* Once app is running, go to [http://localhost:7777/login/](http://localhost:7777/login/).
* Log in with username `test` and password `test` to play a game.
* To end app, kill process in terminal with `CTRL + C`.

## Game Screenshots
<img src="docs/images/login.png" style="width: 400px; height: 350px;" />
<br />
<img src="docs/images/main.png" style="width: 400px; height: 350px;" />
<br />